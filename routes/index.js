/**
 * this file for routing 
 */
const auth = require('./auth');
const frontend = require('./frontend');
const key = require('./key');

module.exports = function(app) {
    auth(app);
    frontend(app);
    key(app);
}