const SettingContainer = require('../controllers/backend/SettingContainer');

module.exports = (app) => {
    app.get('/backend/keys', (req, res) => {
        SettingContainer.index(req, res);
    })

    app.post('/backend/key', (req, res) => {
        SettingContainer.addKey(req, res);
    });
    
    app.put('/backend/key',(req, res) => {
        SettingContainer.updateKey(req, res);
    })
}
