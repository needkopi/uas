/**
 * this is file for routing authentication
 */
const AuthController = require('../controllers/AuthenticationController');

module.exports = (app) => {

    /**
     * login
     */
    app.get('/login', (req, res) => {
        AuthController.login(req, res);
    });

    app.post('/login', (req, res) => {
        AuthController.loged(req, res);
    });

    /**
     * logout
     */
    app.post('/logout', (req, res) => {
        AuthController.logout(req, res);
    });

    /**
     * signup
     */
    app.get('/signup', (req, res) => {
        AuthController.signup(req, res)
    });

    app.post('/signup', function (req, res) {
        AuthController.createUser(req, res);
    });

}