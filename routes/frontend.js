/**
 * this is file for routing authentication
 */
const Home = require('../controllers/frontend/HomeController');

module.exports = (app) => {

    app.get('/', function (req, res) {
        Home.index(req, res);
    });

};