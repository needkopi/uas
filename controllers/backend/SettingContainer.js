const SettingEntity = require('../../modules/entities/SettingEntity');
const Setting = require('../../models/setting');

class SettingContainer {
    
    /*
     * function for get list key
     */
    static async index(req, res){
        let model = await Setting.fetchAll();
        let settings = SettingEntity.normalize(model);

        let json = JSON.stringify({
            status: "success",
            data: {
                setting: settings
            },
            meta: {}
        });

        res.statusCode = 200;
        res.end(json);
    }

    /**
     * function for create key
     */
    static async addKey(req, res) {
        let data = SettingEntity.createSettingNormalize(req.body);

        let is_saved = await Setting.create(data);

        if(!is_saved){
            let json = JSON.stringify({
                status: "error"
            })

            res.statusCode = 500;
            res.end(json);
        }

        let json = JSON.stringify({
            status: "success"
        });

        res.statusCode = 200;
        res.end(json)
    }


    /// Ajax Request
    
    /**
     * Function for upadte key
     */
    static async updateKey(req, res){
        let body = req.body;

        let data = SettingEntity.updateSettingNormalize(body);
        
        let is_saved = await Setting.update(data);
        if(!is_saved){
            let json = JSON.stringify({
                status: "error"
            });

            res.statusCode = 500;
            res.end(json);
        }

        let json = JSON.stringify({
            status: "success"
        });

        res.statusCode = 200;
        res.end(json);
    }

}

module.exports = SettingContainer;
