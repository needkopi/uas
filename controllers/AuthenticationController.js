/**
 * 
 * * this is for authentication user
 * 
 */
const path = require('path');
const UserEntity = require('../modules/entities/UserEntity');
const User = require('../models/user');

class AuthenticationController {
    static login(req, res) {
        res.sendFile(path.join(__dirname, '../resource/views/auth', 'login.html'));
    }

    static async loged(req, res) {
        let body = req.body;
        let user = await User.fetchByEmail(body['email']);
        let valid = await UserEntity.vaidatePassword(user, body['password']);
        res.statusCode = 200;
        res.send('loged');
    }

    static logout(req, res) {
        //code here
    }

    static signup(req, res) {
        res.sendFile(path.join(__dirname, '../resource/views/auth', 'signup.html'));
    }

    static async createUser(req, res) {
        let data = await UserEntity.creaetUserNormalize(req.body);
        if (!data) {
            res.statusCode = 500;
            res.send('error');
        }

        User.create(data);
        res.statusCode = 200;
        res.send('success');
    }
};

module.exports = AuthenticationController;
