const path = require('path');

class HomeController {

    /**
     *  home function
     * 
     * @param req 
     * @param res 
     */
    static index(req, res) {
        let meta = {};
        let data = {
            title: "Product App",
            header: "lest moving..!"
        }
        var json = JSON.stringify({
            status: 'success',
            meta: meta,
            data: data
        });
        res.end(json);
    }
}

module.exports = HomeController;
