const express = require('express');
const route = require('./routes/index');
var bodyParser = require('body-parser')
require('dotenv').config();

const port = process.env.PORT;
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(express.static(__dirname + '/public'));

route(app);

if (process.env.ENV === 'production') console.log = function () { };

console.log('server running on port ', port);
app.listen(port);