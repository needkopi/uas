// const db = require(`./${process.env.DATABASE}`);
const qb = require('./queryBuilder');

class User {

    static create(data) {
        qb.store('users', data);
    }

    static fetchById(id) {
        let sql = qb.select(['name', 'email']) + 'from users ' + qb.where([
            ['id', 1]
        ]);
        let model = qb.first(sql);
    }

    static async fetchByEmail(email) {
        let sql = 'select * from users ' + qb.where([
            ['email', email]
        ]);

        let model = await qb.get(sql);

        let response = {};
        model.forEach(item => {
            response = item;
        });

        return response;
    }

    static fetchAll($options) {
        query = 'select * from users';
        query = this.filterQueryBuilder(query);
        model = qb.get(query);
    }

    filterQueryBuilder(query, options) {
        query = this.filterByStatus(query, options);
        return query;
    }

    filterByStatus(query, options) {
        if (options['status' === 'undefined']) return query;
        return query + `where status = ${options['status']}`;
    }
}

module.exports = User;