require('dotenv').config();

const db = require(`./${process.env.DATABASE}`);
const qb = require('./queryBuilder');

class setting {
    static async create(data) {

        db.connect();

        let query = `INSERT INTO settings ( slug, format, description, file_name ) VALUES ('${data.slug}','${data.format}','${data.desc}', ${data.filename})`;

        let response = await new Promise((resolve, reject) => {
            db.query(query, (err, res) => {
                if (err) reject(false);
                resolve(true);
            });
        });

        db.end();

        return response;

    }

    static fetchById(id) {
        sql = 'select * from settings' + qb.where([['id', 1]]);
        model = qb.first(sql);
    }

    static fetchByKey(key) {
        sql = 'select * from settings' + qb.where([['key', key]]);
        model = qb.first(sql);
    }

    static async fetchAll() {
        let sql = 'select * from settings';
        let model = await qb.get(sql);
        return model;
    }


    static async update(data){
        let query = `update settings set slug='${data.slug}', format='${data.format}', description='${data.desc}' where id=${data.id}`;

        db.connect();

        let response = await new Promise((resolve, reject) => {
            db.query(query, (err, res) => {
                if (err) reject(false);
                resolve(true)
            });
        });

        db.end()

        return response;
    }
}

module.exports = setting;
