require('dotenv').config();

const db = require(`./${process.env.DATABASE}`);

class QueryBuilder {

    static store(table, data) {
        db.connect;
        let column = '';
        let values = '';
        let count = 1;
        let keys = [];
        for (let k in data) keys.push(k);

        keys.forEach(item => {
            if (count == 1) {
                column = `${item}`;
                if (typeof(data[item]) == "string") values = `'${data[item]}'`;
                else values = `${data[item]}`;

            } else {
                column = column + `, ${item}`;
                if (typeof(data[item]) == "string") values = values + `, '${data[item]}'`;
                else values = `${data[item]}`;
            }
            count++;
        });

        db.query(`insert into ${table} (${column}) values (${values})`, (err, res) => {
            if (err) throw err;
        });

        db.end();
    }

    /**
     * 
     * @param {*} table 
     * @param {*} data 
     * @param condition [['id', 1],['name','=>','needkopi']]
     */
    static update(table, data, condition) {
        db.connect;
        let update = '';
        let count = 1;
        let keys = [];
        for (let k in data) keys.push(k);

        keys.forEach(item => {
            if (count == 1) {
                update = `${item} = ${data[item]}`;
            } else {
                update = update + ` ,${item} = ${data[item]}`;
            }
            count++;
        });

        // let str_condition = queryBuilder.where(condition);

        db.query(`update ${table} set ${update} ${str_condition}`, (err, res) => {
            if (err) throw err;
        });

        db.close();
    }

    static select(data) {
        let sql = 'select ';
        let count = 1;

        data.forEach((item) => {
            if (count == 1) {
                sql = sql + `${item}`;
            } else {
                sql = sql + `, ${item}`;
            }

            count++;
        })

        return sql;
    }

    static where(data) {
        let sql = 'where ';
        let count = 1

        data.forEach((item) => {

            if (count == 1) {

                if (item.length == 2) {
                    if (typeof(item[1]) == "string") {
                        sql = sql + ` ${item[0]} = '${item[1]}'`;
                    } else {
                        sql = sql + ` ${item[0]} = ${item[1]}`;
                    }
                } else {
                    if (typeof(item[1]) == "string") {
                        sql = sql + ` ${item[0]} ${item[1]} '${item[2]}'`;
                    } else {
                        sql = sql + ` ${item[0]} ${item[1]} ${item[2]}`;
                    }
                }

            } else {

                if (item.length == 2) {
                    if (typeof(item[1]) == "string") {
                        sql = sql + ` and ${item[0]} = '${item[1]}'`;

                    } else {
                        sql = sql + `and ${item[0]} = ${item[1]}`;
                    }

                } else {
                    if (typeof(item[1]) == "string") {
                        sql = sql + `and ${item[0]} ${item[1]} '${item[2]}'`;

                    } else {
                        sql = sql + `and ${item[0]} ${item[1]} ${item[2]}`;
                    }
                }

            }
            count++;
        });

        return sql;
    }

    static async first(query) {
        db.connect();
        query = query + ' limit = 1';
        let data = await new Promise((resolve, reject) => {
            db.query(query, (err, res) => {
                if (err) reject(err);
                resolve(res);
            });
        });

        db.end();

        return data;
    }

    static async get(query) {
        db.connect();

        let data = await new Promise((resolve, reject) => {
            db.query(query, (err, res) => {
                if (err) reject(err);
                resolve(res);
            })
        });

        db.end();

        return data;
    }
}

module.exports = QueryBuilder;