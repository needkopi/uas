var bcrypt = require('bcrypt');

class UserEntity {

    static async encryptPassword(str_pass) {
        return await new Promise((resolve, reject) => {
            bcrypt.hash(str_pass, 10, (err, hash) => {
                if (err) reject(err)
                resolve(hash);
            });
        })
    }

    static async vaidatePassword(user, str_pass) {
        return await new Promise((resolve, reject) => {
            bcrypt.compare(str_pass, user['password'], (err, res) => {
                if (err) reject(err);
                resolve(res);
            });
        });
    }

    static async creaetUserNormalize(body) {
        if (body.name === 'undefined' || body.email === 'undefined' || body.password === 'undefined') {
            return null;
        }
        let enc_pass = await UserEntity.encryptPassword(body.password);

        return {
            "name": body.name,
            "email": body.email,
            "password": enc_pass,
        };
    }

}

module.exports = UserEntity;