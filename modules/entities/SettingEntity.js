class SettingEntity {
    
    static normalize(model){
        let response = [];
        model.forEach(item => {
            let res = {
                "id"            : item.id,
                "slug"          : item.slug,
                "description"   : item.description
            }

            response.push(res);
        });

        return response;
    }

    static createSettingNormalize(body) {

        return {
            "slug": body.slug,
            "format": body.format,
            "desc": body.desc,
            "filename": null
        }
    }

    static updateSettingNormalize(body){
        return {
            "id"    : body.id,
            "slug"  : body.slug,
            "format": body.format,
            "desc"  : body.desc
        }
    }
}

module.exports = SettingEntity;
